require 'active_support'
require 'active_record'
require 'openssl'
require 'rotp'
require 'rqrcode'

require 'google-authenticator-rails/active_record'

module GoogleAuthenticatorRails
  def self.encryption_supported?
    defined?(Rails)
  end

  # Without this last condition tasks under test are run twice
  if encryption_supported? && !Rails.env.test?
    class Railtie < Rails::Railtie
      rake_tasks do
        load 'tasks/google_authenticator.rake'
      end
    end
  end

  # Drift is set to 6 because ROTP drift is not inclusive. This allows a drift of 5 seconds.
  DRIFT = 6

  def self.time_based_password(secret)
    ROTP::TOTP.new(secret).now
  end

  def self.verify(code, secret, drift = DRIFT, last_otp_at = nil)
    ROTP::TOTP.new(secret).verify(code, drift_behind: drift, after: last_otp_at)
  end

  def self.generate_secret
    ROTP::Base32.random_base32
  end

  def self.secret_encryptor
    @secret_encryptor ||= ActiveSupport::MessageEncryptor.new(
      Rails.application.key_generator.generate_key('Google-secret encryption key', 32)
    )
  end

  module TaskHelper
    PLAINTEXT_LENGTH = 32

    if defined?(Rails)
      if Rails::VERSION::MAJOR < 5 || (Rails::VERSION::MAJOR == 5 && Rails::VERSION::MINOR < 2)
        # Rails 5.2 changed default_cipher for ActiveSupport::MessageEncryptor
        # https://blog.bigbinary.com/2018/06/26/rails-5-2-uses-aes-256-gcm-authenticated-encryption-as-default-cipher-for-encrypting-messages.html
        CIPHERTEXT_LENGTH = 162
      else
        CIPHERTEXT_LENGTH = 100
      end
    end
  end
end
