module GoogleAuthenticatorRails
  module ActiveRecord
    module Helpers
      # Returns and memoizes the plain text google secret for this instance, irrespective of the
      # name of the google secret storage column and whether secret encryption is enabled for this
      # model.
      def google_secret_value
        if @google_secret_value_cached
          @google_secret_value
        else
          @google_secret_value_cached = true
          secret_in_db = google_secret_column_value
          @google_secret_value = unencrypted_secret(secret_in_db) if secret_in_db.present?
        end
      end

      def set_google_secret
        change_google_secret_to!(GoogleAuthenticatorRails.generate_secret)
      end

      # Sets and saves a nil google secret value for this instance.
      def clear_google_secret!
        change_google_secret_to!(nil)
      end

      def google_authentic?(code, prevent_code_reuse = self.class.google_prevent_code_reuse)
        google_last_otp_at = prevent_code_reuse ? google_last_otp_at_column_value : nil

        last_otp_at = GoogleAuthenticatorRails.verify(
          code, google_secret_value, self.class.google_drift, google_last_otp_at
        )

        return false if last_otp_at.nil?

        update_google_last_otp_at!(last_otp_at) if prevent_code_reuse

        true
      end

      def google_qr_png(size: 200)
        RQRCode::QRCode.new(provisioning_uri).as_png(size: size)
      end

      def google_qr_base64(size: 200)
        "data:image/png;base64,#{Base64.strict_encode64(google_qr_png(size: size).to_s)}"
      end

      def google_label
        method = self.class.google_label_method
        case method
        when Proc
          method.call(self)
        when Symbol, String
          __send__(method)
        else
          raise NoMethodError, 'the method used to generate the google_label was never defined'
        end
      end

      def encrypt_google_secret!
        change_google_secret_to!(google_secret_column_value)
      end

      private

      def provisioning_uri
        ROTP::TOTP.new(google_secret_value, issuer: google_issuer).provisioning_uri(google_label)
      end

      def encrypted_secret(secret)
        google_secret_encryptor.encrypt_and_sign(secret)
      end

      def unencrypted_secret(secret_in_db)
        return secret_in_db unless self.class.google_secrets_encrypted

        google_secret_encryptor.decrypt_and_verify(secret_in_db)
      end

      def default_google_label_method
        __send__(self.class.google_label_column)
      end

      def google_secret_column_value
        __send__(self.class.google_secret_column)
      end

      def google_last_otp_at_column_value
        __send__(self.class.google_last_otp_at_column)
      end

      def change_google_secret_to!(secret, encrypt = self.class.google_secrets_encrypted)
        @google_secret_value = secret
        secret_to_db = secret.present? && encrypt ? encrypted_secret(secret) : secret
        __send__("#{self.class.google_secret_column}=", secret_to_db)
        @google_secret_value_cached = true
        save!
      end

      def update_google_last_otp_at!(epoch)
        __send__("#{self.class.google_last_otp_at_column}=", epoch)
        save!
      end

      def google_issuer
        issuer = self.class.google_issuer
        issuer.is_a?(Proc) ? issuer.call(self) : issuer
      end

      def google_secret_encryptor
        GoogleAuthenticatorRails.secret_encryptor
      end
    end
  end
end
