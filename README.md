# GoogleAuthenticatorRails

Rails (ActiveRecord) integration with the Google Authenticator apps for [Android](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2) and the [iPhone](https://itunes.apple.com/us/app/google-authenticator/id388497605?mt=8).

## Developing

See appraisal library for testing multiple rails versions https://github.com/thoughtbot/appraisal
To add a new rails version eddit the `Appraisals` file and run

    bundle exec appraisal install

To run `bundle update` for all appraisals

    bundle exec appraisal update

To run all tests

    bundle exec appraisal rake spec

To run tests without appraisal

    bundle exec rake spec

## Installation

Add this line to your application's Gemfile:

    gem 'google-authenticator-rails'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install google-authenticator-rails

## Usage

Example:

```ruby
class User
  acts_as_google_authenticated
end

@user = User.new
@user.set_google_secret           # => true
@user.google_secret_value         # => 32-character plain-text secret, whatever the name of the secret column
@user.google_authentic?(123456)   # => true
@user.clear_google_secret!        # => true
@user.google_secret_value         # => nil
```

## Google Labels

When setting up an account with `GoogleAuthenticatorRails` you need to provide a label for that account (to distinguish it from other accounts).

`GoogleAuthenticatorRails` allows you to customize how the record will create that label.  There are three options:
  - The default just uses the column `email` on the model
  - You can specify a custom column with the `:column_name` option
  - You can specify a custom method via a symbol or a proc

Example:

```ruby
class User
  acts_as_google_authenticated :column_name => :user_name
end

@user = User.new(:user_name => "ted")
@user.google_label                      # => "ted"

class User
	acts_as_google_authenticated :method => :user_name_with_label

	def user_name_with_label
	  "#{user_name}@example.com"
	end
end

@user = User.new(:user_name => "ted")
@user.google_label                    # => "ted@example.com"

class User
	acts_as_google_authenticated :method => Proc.new { |user| user.user_name_with_label.upcase }

	def user_name_with_label
	  "#{user_name}@example.com"
	end
end

@user = User.new(:user_name => "ted")
@user.google_label                    # => "TED@EXAMPLE.COM"
```

Here's what the labels look like in Google Authenticator for iPhone:

![iPhone Label Screenshot](http://jaredonline.github.io/google-authenticator/images/gar-label.png)

## Google Secret
The "google secret" is where `GoogleAuthenticatorRails` stores the
secret token used to generate the MFA code.

You can also specify a column for storing the google secret.  The default is `google_secret`.

Example

```ruby
class User
	acts_as_google_authenticated :google_secret_column => :mfa_secret
end

@user = User.new
@user.set_google_secret
@user.mfa_secret 		 # => "56ahi483"
```

## Drift

You can specify a custom drift value. Drift is the number of seconds that the client
and server are allowed to drift apart. Default value is 5 seconds.

```ruby
class User
  act_as_google_authenticated :drift => 31
end
```

## Issuer

You can also specify a name for the 'issuer' (the name of the website) where the user is using this token:

Example

```ruby
class User
  acts_as_google_authenticated :issuer => 'example.com'
end
```

You can also use a Proc to set a dynamic issuer for multi-tenant applications or any other custom needs:

```ruby
class User
  acts_as_google_authenticated :issuer => Proc.new { |user| user.admin? ? "Example Admin" : "example.com" }
end
```

This way your user will have the name of your site at the authenticator card besides the current token.

Here's what the issuers look like in Google Authenticator for iPhone:

![iPhone Label Screenshot](http://jaredonline.github.io/google-authenticator/images/gar-issuer.png)

## Sample Rails Setup

This is a very rough outline of how `GoogleAuthenticatorRails` is meant to manage the sessions and cookies for a Rails app.

```ruby
# Gemfile

gem 'rails'
gem 'google-authenticator-rails'
```

First add a field to your user model to hold the Google token.
```ruby
class AddGoogleSecretToUser < ActiveRecord::Migration
  def change
    add_column :users, :google_secret, :string
  end
end
```

```ruby
# app/models/users.rb

class User < ActiveRecord::Base
  acts_as_google_authenticated
end
```

If you want to authenticate based on a model called `User`, then you should name your session object `UserMfaSession`.

```ruby
# app/controllers/user_mfa_session_controller.rb

class UserMfaSessionController < ApplicationController

  def new
    # load your view
  end

  def create
    user = current_user # grab your currently logged in user
    if user.google_authentic?(params[:mfa_code])
      # UserMfaSession.create(user)
      # Handle session outside this gem
      redirect_to root_path
    else
      flash[:error] = "Wrong code"
      render :new
    end
  end

end
```

```ruby
# app/controllers/application_controller.rb

class ApplicationController < ActionController::Base
  before_filter :check_mfa

  private
  def check_mfa
    # Implement without this gem
  end
end
```

## Storing Secrets in Encrypted Form (Rails 4.1 and above)

Normally, if an attacker gets access to the application database, they will be able to generate correct authentication codes,
elmininating the security gains from two-factor authentication. If the application's ```secret_key_base``` is handled more securely
than the database (by, for example, never putting it on the server filesystem), protection against database compromise can
be gained by setting the ```:encrypt_secrets``` option to ```true```. Newly-created secrets will then be stored in encrypted form.

Existing non-encrypted secrets for all models for which the ```:encrypt_secrets``` option has been set to ```true```
can be encrypted by running
```bash
  rails google_authenticator:encrypt_secrets
```
This may be reversed by running
```bash
  rails google_authenticator:decrypt_secrets
```
then by removing, or setting ```false```, the ```:encrypt_secrets``` option.

If ```secret_key_base``` needs to change, set ```old_secret_key_base``` to the old key in ```config/secrets.yml``` before generating the new key.
Then run
```bash
  rails google_authenticator:reencrypt_secrets
```
to change all encrypted google secret fields to use the new key.

If the app is not running under Rails version 4.1 or above, encryption will be disabled, and a warning issued if ```:encrypt_secrets```
is enabled on a model.

If encryption is enabled for a model, the Google secret column of its table must be able to hold at least 162 characters, rather than just 32.

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

## License

MIT.

