ENV['RAILS_ENV'] = 'test'

require 'rails'
require 'byebug'

require 'simplecov'
SimpleCov.start

require 'google-authenticator-rails'

ActiveRecord::Base.establish_connection(
  :adapter  => 'sqlite3',
  :database => ':memory:'
)

ActiveRecord::Schema.define do
  self.verbose = false

  create_table :users, :force => true do |t|
    t.string :google_secret
    t.integer :google_last_otp_at
    t.string :email
    t.string :user_name
    t.string :password
    t.string :salt

    t.timestamps
  end

  create_table :custom_users, :force => true do |t|
    t.string :mfa_secret
    t.string :mfa_last_use
    t.string :email
    t.string :user_name
    t.string :persistence_token

    t.timestamps
  end
end

class BaseUser < ActiveRecord::Base
  # Older versions of ActiveRecord allow attr_accessible, but newer
  # ones do not
  begin
    attr_accessible :email, :user_name, :password
  rescue
    attr_accessor :email, :user_name, :password
  end

  self.table_name = "users"
end

class User < BaseUser
  acts_as_google_authenticated
end

class CustomUser < BaseUser
  self.table_name = "custom_users"
  acts_as_google_authenticated google_secret_column: :mfa_secret, google_last_otp_at_column: :mfa_last_use
end

class NilMethodUser < BaseUser
  acts_as_google_authenticated :method => true
end

class ColumnNameUser < BaseUser
  acts_as_google_authenticated :column_name => :user_name
end

class DriftUser < BaseUser
  acts_as_google_authenticated :drift => 31
end

class ProcLabelUser < BaseUser
  acts_as_google_authenticated :method => Proc.new { |user| "#{user.user_name}@futureadvisor-admin" }
end

class ProcIssuerUser < BaseUser
  acts_as_google_authenticated :issuer => Proc.new { |user| user.admin? ? "FA Admin" :  "FutureAdvisor" }
  def admin?
    true
  end
end

class SymbolUser < BaseUser
  acts_as_google_authenticated :method => :email
end

class StringUser < BaseUser
  acts_as_google_authenticated :method => "email"
end

class QrCodeUser < BaseUser
  acts_as_google_authenticated :method => :email
end

class EncryptedUser < BaseUser
  acts_as_google_authenticated :encrypt_secrets => true
end

class EncryptedCustomUser < BaseUser
  self.table_name = 'custom_users'
  acts_as_google_authenticated :encrypt_secrets => true, :google_secret_column => :mfa_secret
end

class EncryptedUserWithLastOtpColumn < BaseUser
  acts_as_google_authenticated encrypt_secrets: true, prevent_code_reuse: true
end

class UserFactory
  def self.create(klass)
    klass.create(:email => 'test@example.com', :user_name => 'test_user')
  end
end

RAILS_VERSION = "#{Rails::VERSION::MAJOR}.#{Rails::VERSION::MINOR}".to_f

class Application < Rails::Application
  config.load_defaults RAILS_VERSION
end if defined?(Rails)

Rails.application.configure do
  config.eager_load = false
end

Application.initialize!
