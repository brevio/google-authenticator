require 'spec_helper'

describe GoogleAuthenticatorRails do
  context 'time-based passwords' do
    let(:secret)         { 'wvxlooyf4ddvrlqkmyau3wpujnebszlr' }
    let(:original_time)  { Time.parse("2019-05-15T07:45:00Z") }
    let!(:time)          { original_time }
    let(:code)           { "355399" }

    let(:ciphertext_length) { GoogleAuthenticatorRails::TaskHelper::CIPHERTEXT_LENGTH }
    let(:secret_length)     { GoogleAuthenticatorRails::TaskHelper::PLAINTEXT_LENGTH }

    before do
      allow(Time).to receive(:now).and_return(time)
      allow(ROTP::Base32).to receive(:random_base32).and_return(secret)
    end

    specify { expect(GoogleAuthenticatorRails.time_based_password(secret)).to eq(code) }
    specify { expect(GoogleAuthenticatorRails.verify(code, secret)).to be(original_time.to_i) }

    specify { expect(GoogleAuthenticatorRails.verify(code * 2, secret)).to be_nil }
    specify { expect(GoogleAuthenticatorRails.verify(code, secret * 2)).to be_nil }

    if RAILS_VERSION >= 5.2
      specify { expect(ActiveSupport::MessageEncryptor.default_cipher).to eq('aes-256-gcm') }
    end

    it 'can create a secret' do
      expect(GoogleAuthenticatorRails.generate_secret).to eq(secret)
    end

    context 'integration with ActiveRecord' do
      let(:user) { UserFactory.create User }

      before do
        @user = user
        user.google_secret = secret
      end

      context "custom drift" do
        # 30 seconds drift
        let(:user) { UserFactory.create DriftUser }
        subject { user.google_authentic?(code) }

        context '6 seconds of drift' do
          let(:time)  { original_time + 36.seconds }
          it          { should be true }
        end

        context '30 seconds of drift' do
          let(:time)  { original_time + 61.seconds }
          it          { should be false }
        end
      end

      context 'code validation' do
        subject { user.google_authentic?(code) }

        it { is_expected.to be_truthy }

        context 'within 5 seconds of drift' do
          let(:time)  { original_time + 34.seconds }
          it          { is_expected.to be_truthy }
        end

        context '6 seconds of drift' do
          let(:time)  { original_time + 36.seconds }
          it          { is_expected.to be_falsey }
        end
      end

      it 'creates a secret' do
        user.set_google_secret
        expect(user.google_secret).to eq(secret)
      end

      shared_examples 'handles nil secrets' do
        it 'clears a secret' do
          @user.clear_google_secret!
          expect(@user.google_secret_value).to(be_nil) && expect(@user.reload.google_secret_value).to(be_nil)
        end
      end

      it_behaves_like 'handles nil secrets'

      context 'encrypted column' do
        before do
          @user = UserFactory.create EncryptedUser
          @user.set_google_secret
        end

        it 'encrypts_the_secret' do
          expect(@user.google_secret.length).to eq(GoogleAuthenticatorRails.encryption_supported? ? ciphertext_length : secret_length)
        end

        it 'decrypts_the_secret' do
          expect(@user.google_secret_value).to eq(secret)
        end

        it 'validates code' do
          expect(@user.google_authentic?(code)).to be_truthy
        end

        it_behaves_like 'handles nil secrets'
      end

      context 'custom secret column' do
        before do
          @user = UserFactory.create CustomUser
          @user.set_google_secret
        end

        it 'validates code' do
          expect(@user.google_authentic?(code)).to be_truthy
        end
      end

      context 'encrypted column with custom secret column' do
        before do
          @user = UserFactory.create EncryptedCustomUser
          @user.set_google_secret
        end

        it 'encrypts the secret' do
          expect(@user.mfa_secret.length).to eq(GoogleAuthenticatorRails.encryption_supported? ? ciphertext_length : secret_length)
        end

        it 'decrypts the secret' do
          expect(@user.google_secret_value).to eq(secret)
        end

        it 'validates code' do
          expect(@user.google_authentic?(code)).to be_truthy
        end
      end

      context "encrypted column with prevention of code reuse" do
        before do
          @user = UserFactory.create EncryptedUserWithLastOtpColumn
          @user.set_google_secret
          @user.google_authentic?(code)
        end

        it 'sets last use' do
          expect(@user.google_last_otp_at).to eq(time.to_i)
        end

        it 'rejects code reuse' do
          expect(@user.google_authentic?(code)).to be_falsey
        end
      end

      if GoogleAuthenticatorRails.encryption_supported?
        context 'encryption Rake tasks' do
          before(:all) { Rails.application.load_tasks }

          def set_and_run_task(type)
            secret = 'wvxlooyf4ddvrlqkmyau3wpujnebszlr'
            User.delete_all
            EncryptedCustomUser.delete_all
            @user = UserFactory.create User
            @user.set_google_secret
            @encrypted_user = UserFactory.create EncryptedCustomUser
            @encrypted_user.set_google_secret
            @non_encrypted_user = UserFactory.create EncryptedCustomUser
            @non_encrypted_user.update_attribute(:mfa_secret, secret)
            Rake.application.invoke_task("google_authenticator:#{type}_secrets[User,EncryptedCustomUser]")
          end

          def encryption_ok?(user, secret_should_be_encrypted)
            secret_value = user.reload.send(:google_secret_column_value)
            (secret_value.blank? || expect(secret_value.length).to(eq(secret_should_be_encrypted ? ciphertext_length : secret_length))) &&
            (user.class.google_secrets_encrypted ^ secret_should_be_encrypted || user.google_secret_value == secret)
          end

          shared_examples 'task tests' do |type|
            it 'handles non-encrypted secrets' do
              encryption_ok?(@non_encrypted_user, type == 'encrypt')
            end

            it 'handles encrypted secrets' do
              encryption_ok?(@encrypted_user, type != 'decrypt')
            end

            it "doesn't #{type} non-encrypted models" do
              encryption_ok?(@user, false)
            end
          end

          context 'encrypt_secrets task' do
            before(:all) { set_and_run_task('encrypt') }
            it_behaves_like 'task tests', 'encrypt'
          end

          context 'decrypt_secrets task' do
            before(:all) { set_and_run_task('decrypt') }
            it_behaves_like 'task tests', 'decrypt'
          end

          context 'reencrypt_secrets task' do
            before(:all) do
              def reset_encryption(secret_key_base)
                Rails.application.secrets[:secret_key_base] = secret_key_base
                Rails.application.instance_eval { @caching_key_generator = nil }
                GoogleAuthenticatorRails.instance_eval { @secret_encryptor = nil }
              end

              current_secret_key_base = Rails.application.secrets[:secret_key_base]
              reset_encryption(Rails.application.secrets.old_secret_key_base)
              set_and_run_task('reencrypt')
              reset_encryption(current_secret_key_base)
            end

            it_behaves_like 'task tests', 'reencrypt'
          end
        end
      end

      context 'google label' do
        let(:user)    { UserFactory.create NilMethodUser }
        subject       { lambda { user.google_label } }
        it            { should raise_error(NoMethodError) }
      end

      context "drift value" do
        it { expect(DriftUser.google_drift).to eq(31) }

        context "default value" do
          it { expect(User.google_drift).to eq(6) }
        end
      end

      context 'qr codes' do
        let(:user)  { UserFactory.create User }
        before      { user.set_google_secret }

        context 'generates png' do
          let(:user) { UserFactory.create QrCodeUser }
          it { expect(user.google_qr_png.class.eql?(ChunkyPNG::Image)).to be_truthy }
        end

        context 'generates base64 image' do
          let(:user) { UserFactory.create QrCodeUser }
          it { expect(user.google_qr_base64.include?('data:image/png;base64')).to be_truthy }
        end
      end
    end
  end
end
